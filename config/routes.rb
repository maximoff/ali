Rails.application.routes.draw do
  resources :mappings
  get 'snapshots/:source', to: 'mappings#redirect'
  get 'never-guess-url(*)', to: 'frontends#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
