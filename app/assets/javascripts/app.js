// application
var ali = angular.module('ali', ['ngRoute'])
.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'views/index.html',
        controller: 'MappingController'
      }).
      when('/add', {
        templateUrl: 'views/add.html',
        controller: 'MappingController'
      }).
      when('/edit/:id', {
        templateUrl: 'views/edit.html',
        controller: 'MappingController'
      }).
      otherwise({
        redirectTo: '/'
      });
  }])
.run(function() {

});


// controller

ali.controller('MappingController', function($scope, $routeParams, $http, $location) {

	$scope.mappings = [];
	$scope.mapping = {
		source: '',
		target: ''
	}

	$scope.initList = function() {
		$http
			.get('/mappings')
			.success(function(mappings) {
				$scope.mappings = mappings;
			});
	};

	$scope.initEdit = function() {
		$http
			.get('/mappings/' + $routeParams.id)
			.success(function(mapping) {
				$scope.mapping = mapping;
			});
	};

	$scope.add = function() {
		$http
			.post('/mappings', $scope.mapping)
			.success(function(result) {
				$location.path('/');
			});
	};

	$scope.save = function() {
		$http
			.put('/mappings/' + $scope.mapping.id, $scope.mapping)
			.success(function(result) {
				$location.path('/');
			});
	};

	$scope.kill = function(idx) {
		$http
			.delete('/mappings/' + $scope.mappings[idx].id)
			.success(function() {
				$scope.mappings.splice(idx, 1);
			});
	}
     
});
