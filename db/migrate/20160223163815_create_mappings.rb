class CreateMappings < ActiveRecord::Migration[5.0]
  def change
    create_table :mappings do |t|
      t.string :source
      t.string :target

      t.timestamps
    end
  end
end
